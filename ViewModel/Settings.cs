﻿#nullable enable

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Timer = System.Timers.Timer;

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	private IDictionary<string, PropertyInfo>? SettingsMap;

	private Func<string>?   OnLoadData;
	private Action<string>? OnSaveData;

	private Func<Task<string>>? OnLoadDataAsync;
	private Func<string, Task>? OnSaveDataAsync;


	protected virtual void OnSetSaveFunctions( Func<string>? onLoadData, Action<string>? onSaveData )
	{
		OnLoadData = onLoadData;
		OnSaveData = onSaveData;
	}

	protected virtual void OnSetSaveFunctions( Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData )
	{
		OnLoadDataAsync = onLoadData;
		OnSaveDataAsync = onSaveData;
	}

	protected virtual void AfterLoadSettings()
	{
	}

	public async void LoadSettings()
	{
		if( SettingsMap is not null )
		{
			string SaveData;

			if( OnLoadDataAsync is not null )
				SaveData = await OnLoadDataAsync();
			else if( OnLoadData is not null )
				SaveData = OnLoadData();
			else
				return;

			try
			{
				if( SaveData.IsNotNullOrWhiteSpace() )
				{
					var SerializedObject = Encryption.Decrypt( SaveData );
					var Settings         = JsonConvert.DeserializeObject<JObject>( SerializedObject );

					if( Settings is not null )
					{
						lock( SettingsMap )
						{
							// No de-constructor in .net 4.7
							foreach( var Setting in Settings )
							{
								if( SettingsMap.TryGetValue( Setting.Key, out var Info ) )
								{
									try
									{
										dynamic? Value = Setting.Value?.ToObject( Info.PropertyType );
										Info.SetValue( this, Value );
									}
									catch( Exception Exception )
									{
										Console.WriteLine( Exception );
									}
								}
							}
						}
					}
				}
				AfterLoadSettings();
				Refresh();
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
			}
		}
	}

	protected virtual void BeforeSaveSettings()
	{
	}


	public async void SaveSettings()
	{
		if( ( OnSaveData is not null || OnSaveDataAsync is not null ) && SettingsMap is not null && ( SettingsMap.Count > 0 ) )
		{
			if( Dispatcher is not null )
				Dispatcher.Invoke( BeforeSaveSettings );
			else
				BeforeSaveSettings();

			try
			{
				var Settings = new JObject();

				lock( SettingsMap )
				{
					// No de-constructor in .net 4.7
					foreach( var Setting in SettingsMap )
						Settings.Add( new JProperty( Setting.Key, Setting.Value.GetValue( this ) ) );
				}

				var SerializedObject = JsonConvert.SerializeObject( Settings );

				var Data = Encryption.Encrypt( SerializedObject );

				if( OnSaveDataAsync is not null )
					await OnSaveDataAsync( Data );
				else
					OnSaveData?.Invoke( Data );
			}
			catch
			{
			}
		}
	}

	private void InitialiseSettings()
	{
		OnSetSaveFunctions( OnLoadData, OnSaveData );
		SettingsMap = GetAttributeMap( typeof( SettingAttribute ) );
	}

#region Delayed Save
	private          Timer? SaveTimer;
	private readonly object TimerLockObject = new();

	public void SaveSettingsDelayed( int delayInMs = 250 )
	{
		Timer? St;

		lock( TimerLockObject )
		{
			St = SaveTimer;

			if( St is null )
			{
				SaveTimer = St = new Timer( delayInMs )
				                 {
					                 AutoReset = false
				                 };

				St.Elapsed += ( _, _ ) =>
				              {
					              SaveSettings();
				              };
			}
		}
		St.Stop();
		St.Start();
	}
#endregion
}