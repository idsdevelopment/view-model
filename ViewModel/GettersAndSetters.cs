﻿#nullable enable

using System.Linq.Expressions;

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	public Action<Action>? Dispatcher;

	protected T Get<T>( string name, T defaultValue )
	{
		if( TryGetValue( name, out var Value ) )
			return (T)Value!;

		if( defaultValue is not null )
			this[ name ] = defaultValue;

		return defaultValue;
	}

	protected T Get<T>( string name ) => Get( name, default( T )! );

	protected T Get<T>( Expression<Func<T>> expression, T? defaultValue ) => Get( PropertyName( expression ), defaultValue )!;

	protected T Get<T>( string name, Func<T>? initialValue )
	{
		if( TryGetValue( name, out var Value ) )
			return (T)Value!;

		this[ name ] = ( initialValue is not null ? initialValue() : default )!;

		return Get<T>( name );
	}

	protected T Get<T>( Expression<Func<T>> expression, Func<T>? initialValue ) => Get( PropertyName( expression ), initialValue );

	protected void Set<T>( Expression<Func<T>> expression, T value )
	{
		var Name = PropertyName( expression );

		if( TryGetValue( Name, out var Value ) )
		{
			switch( Value )
			{
			case null when value is null:
			case not null when value is not null && Value.Equals( value ):
				return;
			}
		}

		this[ Name ] = value!;
		RaisePropertyChangedDelayed( Name );
	}
}