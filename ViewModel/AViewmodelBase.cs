﻿#nullable enable

using Timer = System.Timers.Timer;

namespace ViewModelsBase;

public abstract partial class AViewModelBase : DynamicDictionary
{
	private readonly Action? _Refresh;
	private readonly object  RefreshTimerLock = new();
	private          Timer?  RefreshTimer;

	public void Refresh()
	{
		lock( RefreshTimerLock )
		{
			if( _Refresh is not null )
			{
				if( RefreshTimer is null )
				{
					RefreshTimer = new Timer( 100 )
					               {
						               AutoReset = false
					               };

					RefreshTimer.Elapsed += ( _, _ ) =>
					                        {
						                        _Refresh?.Invoke();
					                        };
				}
				RefreshTimer.Stop();
				RefreshTimer.Start();
			}
		}
	}

	protected virtual void OnInitialised()
	{
	}

	protected virtual void SetDefaultValues()
	{
		DisableEvents = true;

		try
		{
			InitialiseValues();
			Refresh();
		}
		catch
		{
		}
		finally
		{
			DisableEvents = false;
		}
	}

	protected void ResetValues()
	{
		SetDefaultValues();
	}

	protected virtual void OnFetchValues()
	{
	}

	~AViewModelBase()
	{
		SaveSettings();
	}

	protected AViewModelBase( Action? stateChanged, Func<string>? onLoadData, Action<string>? onSaveData )
	{
		_Refresh   = stateChanged;
		OnSaveData = onSaveData;
		OnLoadData = onLoadData;
		Init();
	}

	protected AViewModelBase( Action? stateChanged, Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData )
	{
		_Refresh        = stateChanged;
		OnSaveDataAsync = onSaveData;
		OnLoadDataAsync = onLoadData;
		Init();
	}


	protected AViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData, Action? osStateChanged ) : this( osStateChanged, onLoadData, onSaveData )
	{
	}

	protected AViewModelBase( Action? stateChanged ) : this( stateChanged, null, null )
	{
	}

	protected AViewModelBase() : this( null, null, (Action?)null )
	{
	}

	protected new object? this[ string ndx ]
	{
		set => base[ ndx ] = value;
		get => base[ ndx ];
	}

	private void Init()
	{
		MapProperties();
		InitialiseSettings();

		// ReSharper disable once VirtualMemberCallInConstructor
		OnFetchValues();

		InitialiseValues();
		LoadSettings();

		MapMethods();
		MapDependsUpon();

		InvokeInitialiseEvents();

		// ReSharper disable once VirtualMemberCallInConstructor
		OnInitialised();
	}
}