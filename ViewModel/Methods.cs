﻿#nullable enable

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	private IEnumerable<MethodInfo>  MethodInfos => _MethodInfos ??= GetType().GetMethods().ToList();
	private IEnumerable<MethodInfo>? _MethodInfos;

	private class Method
	{
		internal int         Delay;
		internal MethodInfo? MethodInfo;
		internal string      Name = "";
	}

	private readonly IDictionary<string, List<Method>> PropertyToDependsUponMethodMap = new Dictionary<string, List<Method>>();
	private readonly IDictionary<string, List<Method>> DependsUponMethodMap           = new Dictionary<string, List<Method>>();

	private readonly IDictionary<string, Timer>      ActiveDelayMap = new Dictionary<string, Timer>();
	protected        IDictionary<string, MethodInfo> MethodMap      = new Dictionary<string, MethodInfo>();


	private void MapMethods()
	{
		var FullName = GetType().FullName;

		// Sometimes has duplicate method name
		MethodMap = ( from M in MethodInfos
		              where !M.IsSpecialName && M.IsPublic && M.DeclaringType is not null && ( M.DeclaringType.FullName == FullName )
		              group M by M.Name
		              into G
		              select G.First() ).ToDictionary( method => method.Name, method => method );
	}

	private void MapDependsUpon()
	{
		foreach( var Method in MethodMap )
		{
			var MethodInfo = Method.Value;

			var Attribs = MethodInfo.GetCustomAttributes( typeof( DependsUponAttribute ), true ).Cast<DependsUponAttribute>();

			var DelayList = ( from A in Attribs
			                  orderby A.Delay descending // Only longest delay get used
			                  select A ).ToList();

			if( DelayList.Count > 0 )
			{
				var Delay = DelayList[ 0 ].Delay;

				var Name = MethodInfo.Name;

				if( !DependsUponMethodMap.TryGetValue( Name, out var MethodList ) )
					DependsUponMethodMap.Add( Name, MethodList = new List<Method>() );

				foreach( var DependsUponAttribute in DelayList )
				{
					var PropertyName = DependsUponAttribute.DependencyName;

					if( !PropertyToDependsUponMethodMap.TryGetValue( PropertyName, out var List ) )
						PropertyToDependsUponMethodMap.Add( PropertyName, List = new List<Method>() );

					var M = new Method
					        {
						        Name       = Name,
						        Delay      = Delay,
						        MethodInfo = MethodInfo
					        };

					List.Add( M );
					MethodList.Add( M );
				}
			}
		}
	}
}