﻿#nullable enable


using System.Diagnostics;

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	protected bool EventsDisabled => DisableEvents;

	protected bool DisableEvents
	{
		get => _DisableEvents > 0;
		set
		{
			if( value )
				++_DisableEvents;
			else if( _DisableEvents > 0 )
				--_DisableEvents;
		}
	}

	protected virtual void OnPropertyChanged( string? name = null )
	{
	}

	private int _DisableEvents;

	private void MethodInvoke( MethodBase? methodInfo )
	{
		if( methodInfo is not null )
		{
			void Change()
			{
				try
				{
					methodInfo.Invoke( this, Array.Empty<object>() );
				}
				catch( Exception E )
				{
					Debug.WriteLine( E );
				}
			}

			if( Dispatcher is not null )
				Dispatcher.Invoke( Change );
			else
				Change();
		}
	}

	public void StopDelay( string functionName )
	{
		if( ActiveDelayMap.TryGetValue( functionName, out var Timer ) )
		{
			ActiveDelayMap.Remove( functionName );
			Timer.Change( Timeout.Infinite, Timeout.Infinite );
			Timer.Dispose();
		}
	}

	public void RaisePropertyChanged( string propertyName )
	{
		if( !DisableEvents )
		{
			void PropertyChanged()
			{
				if( Dispatcher is not null )
				{
					Dispatcher.Invoke( () =>
									   {
										   OnPropertyChanged( propertyName );
									   } );
				}
				else
					OnPropertyChanged( propertyName );

				Refresh();
			}

			if( PropertyToDependsUponMethodMap.TryGetValue( propertyName, out var Methods ) )
			{
				foreach( var Method in Methods )
				{
					var Info = Method.MethodInfo;
					var Name = Method.Name;

					void Invoke()
					{
						MethodInvoke( Info );
					}

					if( Method.Delay > 0 )
					{
						lock( ActiveDelayMap )
						{
							// Restart Timer
							StopDelay( Name );

							ActiveDelayMap[ Name ] = new Timer( _ =>
																{
																	try
																	{
																		lock( ActiveDelayMap )
																		{
																			var T = ActiveDelayMap[ Name ];
																			ActiveDelayMap.Remove( Name );
																			T.Dispose();
																		}

																		Invoke();
																		PropertyChanged();
																	}
																	catch
																	{
																	}
																}, Name, (long)Method.Delay, Timeout.Infinite );
						}
					}
					else
						Invoke();
				}
			}
			PropertyChanged();
		}
	}
}