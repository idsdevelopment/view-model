﻿#nullable enable

using System.Linq.Expressions;
using Timer = System.Timers.Timer;

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	private IEnumerable<PropertyInfo>          PropertyInfos => _PropertyInfos ??= GetType().GetProperties().ToList();
	private IEnumerable<PropertyInfo>?         _PropertyInfos;
	private IDictionary<string, PropertyInfo>? PropertyMap;
	private IDictionary<string, object>?       DefaultValues;

	private readonly IDictionary<string, Timer> ActivePropertyDelayMap = new Dictionary<string, Timer>();
	private readonly IDictionary<string, int>   PropertyDelayMap       = new Dictionary<string, int>();

	public void RaisePropertyChangedDelayed( string propertyName )
	{
		Monitor.Enter( PropertyDelayMap );

		if( PropertyDelayMap.TryGetValue( propertyName, out var Delay ) )
		{
			lock( ActivePropertyDelayMap )
			{
				if( ActivePropertyDelayMap.TryGetValue( propertyName, out var Timer ) )
				{
					Timer.Stop();
					Timer.Start();
					return;
				}

				Timer = new Timer( Delay )
				        {
					        AutoReset = false
				        };

				Timer.Elapsed += ( _, _ ) =>
				                 {
					                 Timer.Stop();
					                 Timer.Dispose();

					                 lock( ActivePropertyDelayMap )
						                 ActivePropertyDelayMap.Remove( propertyName );

					                 RaisePropertyChanged( propertyName );
				                 };

				ActivePropertyDelayMap[ propertyName ] = Timer;
				Timer.Start();
			}
			Monitor.Exit( PropertyDelayMap );
		}
		else
		{
			Monitor.Exit( PropertyDelayMap );
			RaisePropertyChanged( propertyName );
		}
	}


	private static string PropertyName<T>( Expression<Func<T>> expression )
	{
		if( expression.Body is not MemberExpression MemberExpression )
			throw new ArgumentException( "expression must be a property expression" );

		return MemberExpression.Member.Name;
	}

	private void MapProperties()
	{
		var FullName = GetType().FullName;

		PropertyMap = ( from P in PropertyInfos
		                where P.GetMethod is not null && P.GetMethod.IsPublic && P.SetMethod is not null && P.SetMethod.IsPublic && P.DeclaringType is not null && ( P.DeclaringType.FullName == FullName )
		                select P ).ToDictionary( prop => prop.Name, prop => prop );
	}

	private void InitialiseValues()
	{
		if( PropertyMap is not null )
		{
			if( DefaultValues is null )
			{
				DefaultValues = new Dictionary<string, object>();

				// ReSharper disable once UseDeconstruction
				foreach( var Info in PropertyMap )
				{
					var Value = Info.Value.GetValue( this );

					if( Value is not null )
						DefaultValues[ Info.Key ] = Value;
				}
			}

			foreach( var Info in PropertyMap )
			{
				var Key = Info.Key;

				if( DefaultValues.TryGetValue( Key, out var Value ) )
					this[ Key ] = Value;
			}

			// No de-constructor in .net 4.7
			foreach( var Info in GetAttributeMap( typeof( DelayChangeNotification ) ) )
			{
				var Attribs = Info.Value.GetCustomAttributes( typeof( DelayChangeNotification ) ).Cast<DelayChangeNotification>();

				var Attrib = Attribs.FirstOrDefault();

				if( Attrib is not null )
					PropertyDelayMap[ Info.Key ] = Attrib.Delay;
			}
		}
	}
}