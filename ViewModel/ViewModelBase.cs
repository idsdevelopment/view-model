﻿#nullable enable

namespace ViewModelsBase;

public class ViewModelBase : AViewModelBase
{
	protected ViewModelBase( Action? stateChanged ) : base( stateChanged, null, null )
	{
	}

	protected ViewModelBase()
	{
	}

	protected ViewModelBase( Action? stateChanged, Func<string>? onLoadData, Action<string>? onSaveData ) : base( stateChanged, onLoadData, onSaveData )
	{
	}

	protected ViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData ) : base( null, onLoadData, onSaveData )
	{
	}

	protected ViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData, Action? osStateChanged ) : base( osStateChanged, onLoadData, onSaveData )
	{
	}

	protected ViewModelBase( Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData, Action? osStateChanged ) : base( osStateChanged, onLoadData, onSaveData )
	{
	}
}