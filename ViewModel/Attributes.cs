﻿#nullable enable

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	[AttributeUsage( AttributeTargets.Property )]
	protected class InvokeChangeOnInitialise : Attribute
	{
	}

	[AttributeUsage( AttributeTargets.Property )]
	protected class DelayChangeNotification : Attribute
	{
		public int Delay { get; set; }
	}

	[AttributeUsage( AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true )]
	protected class DependsUponAttribute : Attribute
	{
		public string DependencyName { get; }
		public int    Delay          { get; set; }

		public DependsUponAttribute( string propertyName )
		{
			DependencyName = propertyName;
		}
	}

	protected class DependsUpon50Attribute : DependsUponAttribute
	{
		public DependsUpon50Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 50;
		}
	}

	protected class DependsUpon10Attribute : DependsUponAttribute
	{
		public DependsUpon10Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 10;
		}
	}

	protected class DependsUpon100Attribute : DependsUponAttribute
	{
		public DependsUpon100Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 100;
		}
	}


	protected class DependsUpon150Attribute : DependsUponAttribute
	{
		public DependsUpon150Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 150;
		}
	}


	protected class DependsUpon250Attribute : DependsUponAttribute
	{
		public DependsUpon250Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 250;
		}
	}


	protected class DependsUpon350Attribute : DependsUponAttribute
	{
		public DependsUpon350Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 350;
		}
	}


	protected class DependsUpon500Attribute : DependsUponAttribute
	{
		public DependsUpon500Attribute( string propertyName ) : base( propertyName )
		{
			Delay = 500;
		}
	}

	[AttributeUsage( AttributeTargets.Property )]
	protected class SettingAttribute : Attribute
	{
		public string SettingName { get; set; } = "";
	}

	private Dictionary<string, PropertyInfo> GetAttributeMap( Type t )
	{
		return ( from PropertyInfo in PropertyMap
		         let Attr = PropertyInfo.Value.GetCustomAttributes( t, true )
		         where Attr.Length == 1
		         select PropertyInfo ).ToDictionary( prop => prop.Key, prop => prop.Value );
	}
}