﻿#nullable enable

namespace ViewModelsBase;

public partial class AViewModelBase : DynamicDictionary
{
	private void InvokeInitialiseEvents()
	{
		foreach( var PropertyInfo in GetAttributeMap( typeof( InvokeChangeOnInitialise ) ) )
		{
			if( PropertyToDependsUponMethodMap.TryGetValue( PropertyInfo.Key, out var Methods ) )
			{
				foreach( var Method in Methods )
					MethodInvoke( Method.MethodInfo );
			}
		}
	}
}