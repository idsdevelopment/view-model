﻿using XamlViewModel;

namespace MauiViewModel
{
    // All the code in this file is included in all platforms.
    public class ViewModelBase : XamlViewModelBase
    {
        protected override string ProductName => "Maui";

		public static readonly bool IsInDesignMode;

		public new static void Dispatcher(Action a)
		{
			Application.Current!.Dispatcher.Dispatch(a);
		}

		static ViewModelBase()
        {
			IsInDesignMode = DesignMode.IsDesignModeEnabled;
		}

		public ViewModelBase()
		{
			base.Dispatcher = Dispatcher;
		}
	}
}