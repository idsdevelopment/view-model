﻿#nullable enable

using System;
using Xamarin.Forms;

namespace XamlViewModel.Xamarin;

public class ViewModelBase : XamlViewModelBase
{
	public const       string PRODUCT_NAME = "DroidAz";
	public static      bool   IsInDesignMode { get; private set; }
	protected override string ProductName    => PRODUCT_NAME;

	public new static void Dispatcher( Action a )
	{
		Device.BeginInvokeOnMainThread( a );
	}

	public ViewModelBase()
	{
		IsInDesignMode  = DesignMode.IsDesignModeEnabled;
		base.Dispatcher = Dispatcher;
	}
}