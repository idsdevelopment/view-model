﻿#nullable enable

using System;
using System.IO;
using CommandViewModel;
using Utils;
using File = System.IO.File;

namespace XamlViewModel;

public abstract class XamlViewModelBase : ACommandViewModelBase
{
	protected const string SETTINGS_FILE_EXTENSION = "Settings";

	protected abstract string? ProductName { get; }

	private string BaseFolder = "";

	protected string SettingsFolder = "";

	protected string MakeFileName( Type type )
	{
		var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
		BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

		if( !Directory.Exists( BaseFolder ) )
			Directory.CreateDirectory( BaseFolder );

		return $"{BaseFolder}\\{type.Name}.{SETTINGS_FILE_EXTENSION}";
	}

	protected string MakeFileName() => MakeFileName( GetType() );

	protected override void OnSetSaveFunctions( Func<string>? onLoadData, Action<string>? onSaveData )
	{
		base.OnSetSaveFunctions( GetSettings,
		                         SaveSettings
		                       );
	}

	protected XamlViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData ) : base( onLoadData, onSaveData )
	{
	}

	protected XamlViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData, Action? onStateChanged ) : base( onLoadData, onSaveData, onStateChanged )
	{
	}

	protected XamlViewModelBase() : this( null, null )
	{
	}


	private void SaveSettings( string jsonSettings )
	{
		if( jsonSettings.IsNotNullOrEmpty() )
		{
			try
			{
				File.WriteAllText( MakeFileName(), jsonSettings );
			}
			catch
			{
			}
		}
	}

	private string GetSettings()
	{
		try
		{
			return File.ReadAllText( MakeFileName() );
		}
		catch
		{
		}

		return "";
	}
}