﻿#nullable enable

using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Utils;

namespace XamlViewModel.Wpf;

public class ViewModelBase : XamlViewModelBase
{
	public static bool IsInDesignMode
	{
		get
		{
			if( !HaveInDesignMode )
			{
				HaveInDesignMode = true;
				_IsInDesignMode  = DesignerProperties.GetIsInDesignMode( new DependencyObject() );
			}
			return _IsInDesignMode;
		}
	}

	public new static Dispatcher Dispatcher => Application.Current.Dispatcher;

	protected override string? ProductName
	{
		get
		{
			if( _ProductName.IsNullOrEmpty() )
				_ProductName = ( ( Assembly.GetEntryAssembly() ?? throw new InvalidOperationException() ).GetCustomAttributes( typeof( AssemblyProductAttribute ) ).SingleOrDefault() as AssemblyProductAttribute )?.Product;

			return _ProductName;
		}
	}

	private static bool HaveInDesignMode,
	                    _IsInDesignMode;

	private static string? _ProductName = "";

	public virtual void OnArgumentChange( object? arg )
	{
	}
}