﻿#nullable enable

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using CommandViewModel.Annotations;
using ViewModelsBase;
// ReSharper disable ReplaceSubstringWithRangeIndexer

namespace CommandViewModel;

public static class EventExtensions
{
	public static void Raise( this EventHandler eventHandler, object source )
	{
		eventHandler.Invoke( source, EventArgs.Empty );
	}
}

public class Command : ICommand
{
	internal Func<object?, bool> _CanExecute = _ => true;

	internal Action<object> _Execute = _ =>
	                                   {
	                                   };

	internal bool LastCanExecute,
	              First = true,
	              IsAwaitable;

	public void RaiseCanExecuteChanged()
	{
		CanExecuteChanged?.Raise( this );
	}

	public Command( bool isAwaitable )
	{
		IsAwaitable = isAwaitable;
	}


	public bool CanExecute( object? parameter ) => _CanExecute( parameter );

	public void Execute( object? parameter )
	{
		_Execute( parameter! );
	}

	public event EventHandler? CanExecuteChanged;
}

public class ACommandViewModelBase : ViewModelBase, INotifyPropertyChanged
{
	private const string EXECUTE_PREFIX     = "Execute_";
	private const string CAN_EXECUTE_PREFIX = "CanExecute_";

	public readonly Dictionary<string, Command> Commands;


	// Needs to be here else won't override in outer class
	// ReSharper disable once RedundantOverriddenMember
	protected override void OnSetSaveFunctions( Func<string>? onLoadData, Action<string>? onSaveData )
	{
		base.OnSetSaveFunctions( onLoadData, onSaveData );
	}

	[NotifyPropertyChangedInvocator]
	protected override void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	protected ACommandViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData ) : this( onLoadData, onSaveData, null )
	{
	}

	protected ACommandViewModelBase( Func<string>? onLoadData, Action<string>? onSaveData, Action? onStateChanged ) : base( onLoadData, onSaveData, onStateChanged )
	{
		var NewMethodMap     = new Dictionary<string, MethodInfo>();
		var DelegateCommands = new Dictionary<string, Command>();

		// ReSharper disable once UseDeconstruction
		foreach( var MethodInfo in MethodMap )
		{
			var Key         = MethodInfo.Key;
			var Value       = MethodInfo.Value;
			var IsAwaitable = Value.ReturnType.GetMethod( nameof( Task.GetAwaiter ) ) is not null;

			if( Key.StartsWith( EXECUTE_PREFIX ) )
			{
				var Method = Key.Substring( EXECUTE_PREFIX.Length );

				if( !DelegateCommands.TryGetValue( Method, out var M ) )
					DelegateCommands.Add( Method, M = new Command( IsAwaitable ) );

				M._Execute = parameter =>
				             {
					             Value.Invoke( this, Value.GetParameters().Length == 1 ? new[] {parameter} : null );
				             };
			}

			else if( Key.StartsWith( CAN_EXECUTE_PREFIX ) )
			{
				var Method = Key.Substring( CAN_EXECUTE_PREFIX.Length );

				if( !DelegateCommands.TryGetValue( Method, out var M ) )
					DelegateCommands.Add( Method, M = new Command( IsAwaitable ) );

				M._CanExecute = parameter =>
				                {
					                var RetVal = false;

					                try
					                {
						                var Temp = Value.Invoke( this, Value.GetParameters().Length == 1 ? new[] {parameter} : null );

						                RetVal = Temp is true;

						                if( M.First || ( M.LastCanExecute != RetVal ) )
						                {
							                M.First          = false;
							                M.LastCanExecute = RetVal;
							                M.RaiseCanExecuteChanged();
						                }
					                }
					                catch
					                {
					                }
					                return RetVal;
				                };
			}
			else
				NewMethodMap.Add( Key, MethodInfo.Value );
		}

		MethodMap = NewMethodMap;
		Commands  = DelegateCommands;

		// So Framework sees ICommands
		foreach( var Command in DelegateCommands )
			this[ Command.Key ] = Command.Value;
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}